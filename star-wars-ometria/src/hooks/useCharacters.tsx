import { useState, useEffect } from "react";
import axios from "axios";

export type ResponseType = {
  count?: number;
  next?: string;
  previous?: string;
  results?: Character_Response[];
};

export type Character = {
  name: string;
  birth_year: string;
  eye_color: string;
  gender: string;
  hair_color: string;
  height: string;
  mass: string;
  skin_color: string;
};
export type Character_Response = {
  homeworld: string;
  films: string[];
  species: string[];
  starships: string[];
  vehicles: string[];
  url: string;
  created: string;
  edited: string;
} & Character;

const BASE_URL = "https://swapi.dev/api/people/";
const SEARCH_URL_PARAM = "search=";
const PAGE_URL_PARAM = "page=";

const urlBuilder = (page: number, searchTerm?: string) => {
  let url = BASE_URL + "?";
  if (!!searchTerm) {
    url += SEARCH_URL_PARAM + searchTerm + "&";
  }
  url += PAGE_URL_PARAM + page;

  return url;
};

export const useCharacters = (page: number, searchTerm?: string) => {
  const [data, setData] = useState<ResponseType>();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const response = await axios.get<ResponseType>(
          urlBuilder(page, searchTerm)
        );
        setData(response.data);
        setIsLoading(false);
      } catch (e) {
        setError("Error getting the data");
      }
    };

    fetchData();
  }, [page, searchTerm]);

  return { results: data?.results, count: data?.count, isLoading, error };
};
