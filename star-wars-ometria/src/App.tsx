import { useState } from "react";
import "./App.css";
import Table from "./components/table/Table";
import { useCharacters } from "./hooks/useCharacters";
import { Pagination, debounce } from "@mui/material";
import Search from "./components/search/Search";
import Loader from "./components/loader/Loader";

const PAGE_SIZE = 10;

function App() {
  const [page, setPage] = useState(1);
  const [searchTerm, setSearchTerm] = useState("");
  const { results, count, error, isLoading } = useCharacters(page, searchTerm);

  if (isLoading && !results) {
    return <Loader />;
  } else if (error || !results) {
    return <div>{error}</div>;
  }

  const handleSearchChange = (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setSearchTerm(event.target.value);
    setPage(1);
  };
  const debouncedOnChange = debounce(handleSearchChange, 300);

  return (
    <div
      className="App"
      style={{
        display: "flex",
        flexDirection: "column",
        gap: "16px",
      }}
    >
      {isLoading && <Loader />}
      <Search onChange={debouncedOnChange} />
      {count === 0 ? <p>NO RESULTS</p> : <Table results={results} />}
      {!!count && (
        <Pagination
          count={Math.ceil(count / PAGE_SIZE)}
          page={page}
          onChange={(_, value) => setPage(value)}
          style={{ alignSelf: "center" }}
        />
      )}
    </div>
  );
}

export default App;
