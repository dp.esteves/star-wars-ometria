import { ChangeEventHandler } from "react";
import { Input } from "@mui/material";

const Search: React.FC<{
  onChange: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>;
}> = ({ onChange }) => {
  return (
    <div style={{ width: "70%", marginTop: "24px", alignSelf: "center" }}>
      <Input
        name="search-field"
        onChange={onChange}
        placeholder="Search by name..."
        fullWidth
      />
    </div>
  );
};

export default Search;
