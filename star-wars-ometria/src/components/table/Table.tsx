import MUITable from "@mui/material/Table";
import MUITableBody from "@mui/material/TableBody";
import MUITableHead from "@mui/material/TableHead";
import MUITableRow from "@mui/material/TableRow";
import MUITableCell from "@mui/material/TableCell";

import { Character_Response } from "../../hooks/useCharacters";
import TableLine from "./TableLine";

const Table: React.FC<{ results: Character_Response[] }> = ({ results }) => {
  const {
    url,
    edited,
    created,
    starships,
    vehicles,
    species,
    films,
    homeworld,
    ...keys
  } = results[0];

  return (
    <MUITable sx={{ minWidth: 1000 }}>
      <MUITableHead>
        <MUITableRow>
          {Object.entries(keys).map(([key]) => (
            <MUITableCell key={key}>{key.split("_").join(" ")}</MUITableCell>
          ))}
        </MUITableRow>
      </MUITableHead>
      <MUITableBody>
        {results.map((c, index) => {
          const {
            url,
            edited,
            created,
            starships,
            vehicles,
            species,
            films,
            homeworld,
            ...character
          } = c;
          return <TableLine character={character} key={`${c.name}-${index}`} />;
        })}
      </MUITableBody>
    </MUITable>
  );
};

export default Table;
