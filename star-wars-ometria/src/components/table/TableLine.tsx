import axios from "axios";

import MUITableRow from "@mui/material/TableRow";
import MUITableCell from "@mui/material/TableCell";

import { Character } from "../../hooks/useCharacters";

const TableLine: React.FC<{ character: Character }> = ({ character }) => {
  return (
    <MUITableRow>
      {Object.entries(character).map(([key, value]) => (
        <MUITableCell key={`${character?.name}-${key}`}>
          {Array.isArray(value) ? value.join(", ") : value}
        </MUITableCell>
      ))}
    </MUITableRow>
  );
};

export default TableLine;
